package com.apps.smsblaster.ui.setup.counting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apps.smsblaster.R;
import com.apps.smsblaster.ui.detailorder.DetailOrderActivity;
import com.apps.smsblaster.ui.setup.message.SetupMessageActivity;
import com.apps.smsblaster.ui.setup.operator.SetupOperatorActivity;

public class SetupCountingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_counting);

        Button playPause2 = (Button)findViewById(R.id.onNext);

        playPause2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext2();
            }
        });

    }
    public void onNext1(){
        startActivity(new Intent(SetupCountingActivity.this, SetupMessageActivity.class));
    }
    public void onNext2(){
        startActivity(new Intent(SetupCountingActivity.this, DetailOrderActivity.class));
    }
}
