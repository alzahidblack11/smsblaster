package com.apps.smsblaster.ui.setup.message;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apps.smsblaster.R;
import com.apps.smsblaster.ui.menu.MenuActivity;
import com.apps.smsblaster.ui.setup.counting.SetupCountingActivity;
import com.apps.smsblaster.ui.setup.operator.SetupOperatorActivity;

public class SetupMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_message);
        Button playPause = (Button)findViewById(R.id.onPrev);

        playPause.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext1();
            }
        });
        Button playPause2 = (Button)findViewById(R.id.onNext);

        playPause2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext2();
            }
        });

    }
    public void onNext1(){
        startActivity(new Intent(SetupMessageActivity.this, SetupOperatorActivity.class));
    }
    public void onNext2(){
        startActivity(new Intent(SetupMessageActivity.this, SetupCountingActivity.class));
    }
}
