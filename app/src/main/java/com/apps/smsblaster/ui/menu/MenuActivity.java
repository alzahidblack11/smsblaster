package com.apps.smsblaster.ui.menu;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.smsblaster.R;
import com.apps.smsblaster.ui.login.LoginActivity;
import com.apps.smsblaster.ui.number.NumberActivity;
import com.apps.smsblaster.ui.onesetup.operator.OneOperatorActivity;
import com.apps.smsblaster.ui.orderlist.OrderActivity;
import com.apps.smsblaster.ui.setting.operator.SettingOperatorActivity;
import com.apps.smsblaster.ui.setting.setting.SettingActivity;
import com.apps.smsblaster.ui.setup.operator.SetupOperatorActivity;

import java.util.HashMap;

public class MenuActivity extends AppCompatActivity {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Toolbar toolbar;

    // Inflate the layout for this fragment



    // urls to load navigation header background image
    // and profile image

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments


    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header


        // load toolbar titles from string resources
        // load toolbar titles from string resources

        LinearLayout playPause = (LinearLayout)findViewById(R.id.nex1);

        playPause.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext1();
            }
        });
        LinearLayout playPause2 = (LinearLayout)findViewById(R.id.nex2);

        playPause2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext2();
            }
        });
        LinearLayout playPause3 = (LinearLayout)findViewById(R.id.nex3);

        playPause3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext3();
            }
        });
        LinearLayout playPause4 = (LinearLayout)findViewById(R.id.nex4);

        playPause4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext4();
            }
        });
        // load nav menu header data();

        // initializing navigation menu
        setUpNavigationView();

    }


    @SuppressLint("RestrictedApi")
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // show or hide the fab button

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
    }

    public void onNext1(){
        startActivity(new Intent(MenuActivity.this, SetupOperatorActivity.class));
    }
    public void onNext2(){
        startActivity(new Intent(MenuActivity.this, OneOperatorActivity.class));
    }
    public void onNext3(){
        startActivity(new Intent(MenuActivity.this, OrderActivity.class));
    }
    public void onNext4(){
        startActivity(new Intent(MenuActivity.this, SettingOperatorActivity.class));
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                //Check to see which item was being clicked and perform appropriate action


                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_menu:
                        navItemIndex = 0;
                        startActivity(new Intent(MenuActivity.this, MenuActivity.class));
                        drawer.closeDrawers();


                        break;
                    case R.id.nav_setup:
                        navItemIndex = 1;
                        startActivity(new Intent(MenuActivity.this, SetupOperatorActivity.class));

                        drawer.closeDrawers();

                        break;
                    case R.id.nav_one_setup:
                        navItemIndex = 2;
                        startActivity(new Intent(MenuActivity.this, OneOperatorActivity.class));

                        drawer.closeDrawers();

                        break;
                    case R.id.nav_order:
                        navItemIndex = 3;
                        startActivity(new Intent(MenuActivity.this, OrderActivity.class));

                        drawer.closeDrawers();

                        break;
                    case R.id.nav_setting:
                        navItemIndex = 4;
                        startActivity(new Intent(MenuActivity.this, SettingOperatorActivity.class));

                        drawer.closeDrawers();

                        break;
                    case R.id.nav_no:
                        navItemIndex = 5;
                        startActivity(new Intent(MenuActivity.this, NumberActivity.class));

                        drawer.closeDrawers();

                        break;

                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home

        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected

        MenuInflater inflater = getMenuInflater();


        inflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

}
