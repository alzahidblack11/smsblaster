package com.apps.smsblaster.ui.onesetup.counting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apps.smsblaster.R;
import com.apps.smsblaster.ui.menu.MenuActivity;
import com.apps.smsblaster.ui.onesetup.operator.OneOperatorActivity;
import com.apps.smsblaster.ui.setting.operator.SettingOperatorActivity;
import com.apps.smsblaster.ui.setting.setting.SettingActivity;

public class OneCountingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_counting);
        Button playPause2 = (Button)findViewById(R.id.onNext);

        playPause2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext2();
            }
        });

    }
    public void onNext1(){
        startActivity(new Intent(OneCountingActivity.this, OneOperatorActivity.class));
    }
    public void onNext2(){
        startActivity(new Intent(OneCountingActivity.this, MenuActivity.class));
    }
}
