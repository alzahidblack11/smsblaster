package com.apps.smsblaster.ui.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apps.smsblaster.R;
import com.apps.smsblaster.ui.menu.MenuActivity;
import com.apps.smsblaster.ui.onesetup.operator.OneOperatorActivity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button playPause = (Button)findViewById(R.id.button);

        playPause.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                onNext();
            }
        });
    }
    public void onNext(){
        startActivity(new Intent(LoginActivity.this, MenuActivity.class));
    }
}
